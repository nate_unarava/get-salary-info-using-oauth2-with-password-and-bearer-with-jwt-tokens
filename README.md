# About

This REST-API is a test task project made for applying to CFT Python backend development course.

It receives user's login and password and gives back a JWT token. Token expiry time: 30 minutes. Using this token, user can get his/her salary and promotion date information.

***

Данный REST-сервис является проектом тестового задания для зачисления на курс "Разработка backend сервисов на Python". 

Данный API получает логин и пароль сотрудника и выдает JWT токен. Время действия токена: 30 минут. Используя этот токен, сотрудник может получить его/ее информацию о запралате и дату повышения.

# TechStack
- Python 3.10.11
- FastAPI 0.95.2
- Python-JOSE 3.3.0 
- Pydantic 1.10.8
- uvicorn 0.22.0

# Setup

Clone this repo
```
git clone https://gitlab.com/nate_unarava/get-salary-info-using-oauth2-with-password-and-bearer-with-jwt-tokens.git
```
Use terminal to install TechStack:
```
pip3 install datetime
pip3 install typing
pip3 install "fastapi[all]"
pip3 install "python-jose[cryptography]"
pip3 install "passlib[bcrypt]"
pip3 install pydantic
pip3 install "uvicorn[standard]"
pip3 install python-multipart
```

# API Docs
## Swagger - OpenAPI

To start uvicorn type the following in bash:
```
python3 -m uvicorn main:app --reload 
```

Open your browser at:

http://127.0.0.1:8000/docs#/

You will see the automatic interactive API documentation (provided by [Swagger UI](https://github.com/swagger-api/swagger-ui))

## Endpoints

| Method | Endpoint       | Access Control | Description |
|--------|----------------|----------------|-------------|
| POST   | /token         | all users      | Login with a registered account.|
| GET    | /users/me      | all users      | See user's info.|
| GET    | /users/me/info/| all users      | Read user's salary and promotion info.|

# Data Model
There is a database contains information about users such as below:
```
{
    "username": "natalliaunarava",
    "full_name": "Наталья Унарова",
    "email": "natalieunarava@gmail.com",
    "hashed_password": "$2b$12$8q6b4Qp1OWPvAm6tVN/rRuG8D3sYQua59hWOR/fDyOu4obJ0nVhCq",
    "salary": float(19500.15),
    "currency": "rub",
    "promotion_date": "20.08.2023",
    "disabled": False
}
```

# Operation POST with the path /token
The function *login_for_access_token* recieves username and password to authorize. It authenticates the user by checking username correctness and verifying entered password with hashed password in database. If something is wrong, it raises an error (HTTP_401_UNAUTHORIZED Incorrect username or password). If verification is passed, then it returns access token:
```
{
  "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJuYXRhbGxpYXVuYXJhdmEiLCJleHAiOjE2ODY1MDQwMDJ9.MhG_9VVPQzohbagxeT3K30SrLY7UwAlilc2N7bLnUCg",
  "token_type": "bearer"
}
```

# Operation GET with the path /users/me
The function *read_users_me* returns current active user data. The user must be successfully loged in otherwise there is an error (Unauthorized Not authenticated). Also if parameter "disabled" is True (which means the user is inactive) it rises error (Inactive user). Successful response contains following response body:
```
{
  "username": "natalliaunarava",
  "email": "natalieunarava@gmail.com",
  "full_name": "Наталья Унарова",
  "salary": 19500.15,
  "currency": "rub",
  "promotion_date": "20.08.2023",
  "disabled": false
}
```

# Operation GET with path /users/me/info
The function *read_own_info* returns current active user's name, salary, currency, promotion date information. There is the same mechanism as in operation upper. The user must be successfully loged in otherwise there is an error (Unauthorized Not authenticated). Also if parametr "disabled" is True (which means the user is inactive) it rises error (Inactive user). Successful response contains following response body:
```
  {
    "name": "Наталья Унарова",
    "salary": 19500.15,
    "currency": "rub",
    "promotion_date": "20.08.2023"
  }
```

# Manual test input
To demonstrate the way it works this example has users database which contains 2 profiles. 
The first is active user.

Username: 
```
natalliaunarava
```
Password:
```
strongpassword
```
The second user is inactive.

Username:
```
vitamaslova
```
Password:
```
ilikebooks
```